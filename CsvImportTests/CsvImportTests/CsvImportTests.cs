﻿using CsvImportBAL.CsvToXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using CsvImportBAL.Exceptions;
using Shouldly;
using Xunit;

namespace CsvImportTests.CsvImportTests
{
    public class CsvImportTests:ICurrency
    {
        private readonly ICsvToXml _converter;

        public CsvImportTests()
        {
            this._converter = new CsvToXml(this);
        }

        //[Theory]
        //[InlineData("C:\\tests\\CandidateTest_Example1.csv")]
        //take path from current directory
        public void CsvToXmlShouldWorkForValidCsv(string path)
        {
            var success = _converter.Process(path);
            success.ShouldBe(true);
        }

        //[Theory]
        //[InlineData("C:\\tests\\CandidateTest_Example2.csv")]
        //take path from current directory
        public void CsvToXmlShouldLogFailureForInValidCsv(string path)
        {
            var succ = _converter.Process(path);
            succ.ShouldBe(false);
        }

        public decimal ToGbp(decimal value, string currency)
        {
            throw new NotImplementedException();
        }
    }
}
