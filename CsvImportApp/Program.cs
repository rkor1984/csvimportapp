﻿using CsvImportBAL.CsvToXml;
using Microsoft.Extensions.DependencyInjection;

namespace CsvImportApp
{
    class Program
    {
        static void Main(string[] args)
        {
            //setup our DI
            var services = ConfigureServices();
            var serviceProvider = services.BuildServiceProvider();
            serviceProvider.GetService<IConsoleApplication>().Run(args);
        }
        private static IServiceCollection ConfigureServices()
        {
            IServiceCollection services = new ServiceCollection();
            services.AddTransient<ICurrency, Currency>();
            services.AddTransient<ICsvToXml, CsvToXml>();
            // IMPORTANT! Register our application entry point
            services.AddTransient<IConsoleApplication, ConsoleApplication>();
            return services;
        }
    }
}
