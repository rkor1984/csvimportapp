﻿using System;
using System.IO;
using CsvImportBAL.CsvToXml;

namespace CsvImportApp
{
    class ConsoleApplication : IConsoleApplication
    {
        private  readonly ICsvToXml _csvToXml;

        public ConsoleApplication(ICsvToXml csvToXml)
        {
            _csvToXml = csvToXml;
        }
        public void Run(string[] args)
        {
            foreach (var path in args)
            {
                if (Directory.Exists(path))
                {
                    Console.WriteLine("{0} is  a valid file or directory.", path);
                    Console.WriteLine($"Watching:  {path}");
                    ProcessDirectory(path);
                }
                else
                {
                    Console.WriteLine("{0} is not a valid file or directory.", path);
                }
            }

        }

        private void ProcessDirectory(string path)
        {
            using (var watcher = new FileSystemWatcher())
            {
                watcher.Path = path;
                watcher.NotifyFilter = NotifyFilters.LastAccess
                                       | NotifyFilters.LastWrite
                                       | NotifyFilters.FileName
                                       | NotifyFilters.DirectoryName;

                watcher.Filter = "*.csv";

                watcher.Changed += OnChanged;
                watcher.Created += OnChanged;
                watcher.Renamed += OnRenamed;

                watcher.EnableRaisingEvents = true;

                Console.WriteLine("Press 'q' to quit the sample.");
                while (Console.Read() != 'q') ;
            }
        }

        private void OnChanged(object sender, FileSystemEventArgs e)
        {
            try
            {
                var success = _csvToXml.Process(e.FullPath);

                if (success)
                {
                    Console.WriteLine($"{e.FullPath} is converted to xml");
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
        }

        private  void OnRenamed(object sender, RenamedEventArgs e)
        {
            Console.WriteLine(e.FullPath + " " + e.ChangeType + " " + e.Name);
        }
    }
}
