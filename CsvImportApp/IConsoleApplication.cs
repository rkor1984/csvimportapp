﻿namespace CsvImportApp
{
    internal interface IConsoleApplication
    {
        void Run(string[] args);
    }
}