﻿using System;
using System.Runtime.Serialization;

namespace CsvImportBAL.Exceptions
{
    [Serializable]
    internal class InValidDataProvidedException : Exception
    {
        public InValidDataProvidedException()
        {
        }

        public InValidDataProvidedException(string message) : base(message)
        {
        }

        public InValidDataProvidedException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InValidDataProvidedException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}