﻿using System.Collections.Generic;

namespace CsvImportBAL.Mappings
{
    internal class OrderMap
    {
        public OrderMap(string header)
        {
            string[] headerArray=header.Split(',');
            for (var i = 0; i < headerArray.Length; i++)
            {
                OrderMapDictionary.Add(headerArray[i].Trim().ToLower(),i);
            }
        }

        public Dictionary<string,int> OrderMapDictionary { get; set; }=new Dictionary<string, int>();
    }
}