﻿using CsvImportBAL.CsvToXml;
using CsvImportBAL.Exceptions;

namespace CsvImportBAL.Validation
{
    public class Validator
    {
        public Validator()
        {
        }

        public decimal ValidateDecimal(string value)
        {
            var w = 0m;
            var validValue = decimal.TryParse(value, out w);
            if (validValue)
            {
                return w;
            }
            else
            {
                throw new InValidDataProvidedException("Value: Data provided is Invalid");
            }
        }
    }
}