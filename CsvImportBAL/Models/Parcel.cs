﻿using System.Linq;
using System.Xml.Serialization;

namespace CsvImportBAL.Models
{
    [XmlRoot(ElementName = "Parcel")]
    public class Parcel
    {
        //needed for serialization
        public Parcel()
        {

        }

        public Parcel(IGrouping<string, ParcelItemViewModel> parcelGroup)
        {
            ParcelCode = parcelGroup.Key;
            var parcelAddressDetails = parcelGroup.ToList().FirstOrDefault();
            if (parcelAddressDetails != null)
            {
                Address1 = parcelAddressDetails.Address1;
                Address2 = parcelAddressDetails.Address2;
                City = parcelAddressDetails.City;
                CountryCode = parcelAddressDetails.CountryCode;
                State = parcelAddressDetails.State;
            }
            ParcelItems=new ParcelItems(parcelGroup);
            
        }

        [XmlElement(ElementName = "ParcelItems")]
        public ParcelItems ParcelItems { get;  set; } 

        [XmlElement(ElementName = "ParcelCode", DataType = "string")]
        public string ParcelCode { get;  set; }

        [XmlElement(ElementName = "Address1", DataType = "string")]
        public string Address1 { get;  set; }

        [XmlElement(ElementName = "Address2", DataType = "string")]
        public string Address2 { get;  set; }

        [XmlElement(ElementName = "City", DataType = "string")]
        public string City { get;  set; }

        [XmlElement(ElementName = "CountryCode", DataType = "string")]
        public string CountryCode { get;  set; }

        [XmlElement(ElementName = "State", DataType = "string")]
        public string State { get;  set; }
    }
}