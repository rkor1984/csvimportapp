﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace CsvImportBAL.Models
{
    [XmlRoot(ElementName = "Orders")]
    public class Orders
    {
        [XmlElement(ElementName = "Order")]
        public List<Order> OrderList { get; set; }

        [XmlElement(ElementName = "TotalValue", DataType = "decimal")]
        public decimal TotalValue { get; set; }

        [XmlElement(ElementName = "TotalWeight", DataType = "decimal")]
        public decimal TotalWeight { get; set; }
    }
}