﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace CsvImportBAL.Models
{
    [XmlRoot(ElementName = "Consignments")]
    public class Consignments
    {
        //needed for serialization
        public Consignments()
        {

        }

        public Consignments(IEnumerable<IGrouping<string, ParcelItemViewModel>> consignmentGroup)
        {
            foreach (var consignment in consignmentGroup)
            {
                this.Consignment.Add(new Consignment(consignment));
            }
        }

        [XmlElement(ElementName = "Consignment")]
        public List<Consignment> Consignment { get; set; } = new List<Consignment>();
    }
}