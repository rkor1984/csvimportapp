﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using CsvImportBAL.Validation;

namespace CsvImportBAL.Models
{
    [XmlRoot(ElementName = "ParcelItem")]
    public class ParcelItem
    {
        //needed for serialization
        public ParcelItem()
        {
        }

        public ParcelItem(string[] parcelItem, Dictionary<string, int> mapping)
        {
            var validator = new Validator();
            this.Quantity = int.Parse(parcelItem[mapping["item quantity"]]);
            this.Value = decimal.Parse(parcelItem[mapping["item value"]]);
            Weight= validator.ValidateDecimal(parcelItem[mapping["item weight"]]);
            Value= validator.ValidateDecimal(parcelItem[mapping["item value"]]);
            this.Description = parcelItem[mapping["item description"]];
            this.Currency = parcelItem[mapping["item currency"]] == "" ? "GBP" : parcelItem[mapping["item currency"]];

        }

        [XmlElement(ElementName = "ItemWeight", DataType = "decimal")]
        public decimal Weight { get; set; }

        [XmlElement(ElementName = "ItemDescription")]
        public string Description { get; set; }

        [XmlElement(ElementName = "ItemCurrency")]
        public string Currency { get; set; }
        [XmlElement(ElementName = "ItemValue", DataType = "decimal")]
        public decimal Value { get; set; }

        [XmlElement(ElementName = "ItemQuantity", DataType = "int")]
        public int Quantity { get; set; }
    }

    [XmlRoot(ElementName = "ParcelItems")]
    public class ParcelItems
    {
        public ParcelItems()
        {

        }

        public ParcelItems(IGrouping<string, ParcelItemViewModel> parcelGroup)
        {
            foreach (var parcelItem in parcelGroup.ToList())
            {
                Item.Add(parcelItem.Item);
            }
        }

        [XmlElement(ElementName = "ParcelItem")]
        public List<ParcelItem> Item { get; set; } = new List<ParcelItem>();

    }
}