﻿using System.Collections.Generic;

namespace CsvImportBAL.Models
{
    public class ParcelItemViewModel
    {
        public ParcelItemViewModel(string[] parcelItem, Dictionary<string, int> mapping)
        {
            Item = new ParcelItem(parcelItem, mapping);
            OrderNumber = parcelItem[mapping["order no"]];
            ConsignmentNumber = parcelItem[mapping["consignment no"]];
            ParcelNumber = parcelItem[mapping["parcel code"]];
            Address1 = parcelItem[mapping["address 1"]];
            Address2 = parcelItem[mapping["address 2"]];
            City = parcelItem[mapping["city"]];
            State = parcelItem[mapping["state"]];
            CountryCode= parcelItem[mapping["country code"]];
            ConsigneeName = parcelItem[mapping["consignee name"]];
        }

        public ParcelItem Item { get; private set; }
        public string OrderNumber { get; private set; }
        public string ConsignmentNumber { get; private set; }
        public string ParcelNumber { get; private set; }
        public string Address1 { get; private set; }
        public string Address2 { get; private set; }
        public string City { get; private set; }
        public string State { get; private set; }
        public string CountryCode { get; private set; }
        public string ConsigneeName { get; internal set; }
    }
}