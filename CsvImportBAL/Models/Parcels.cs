﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using CsvImportBAL.CsvToXml;

namespace CsvImportBAL.Models
{
    [XmlRoot(ElementName = "Parcels")]
    public class Parcels
    {
        //needed for serialization
        public Parcels()
        {

        }

        public Parcels(IEnumerable<IGrouping<string, ParcelItemViewModel>> parcelGroups)
        {
            foreach (var parcelGroup in parcelGroups)
            {
                Parcel.Add(new Parcel(parcelGroup));
            }
        }

        [XmlElement(ElementName = "Parcel")]
        public List<Parcel> Parcel { get; set; } = new List<Parcel>();
    }
}