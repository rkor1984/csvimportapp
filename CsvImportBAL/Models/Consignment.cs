﻿using System.Linq;
using System.Xml.Serialization;

namespace CsvImportBAL.Models
{
    [XmlRoot(ElementName = "Consignment")]
    public class Consignment
    {

        //needed for serialization
        public Consignment()
        {

        }

        public Consignment(IGrouping<string, ParcelItemViewModel> consignment)
        {
            this.ConsignmentNumber = consignment.Key;
            this.ConsigneeName = consignment.ToList().FirstOrDefault()?.ConsigneeName;
            var parcelGroups = consignment.ToList().GroupBy(x => x.ParcelNumber);
            Parcels=new Parcels(parcelGroups);
            
        }

        [XmlElement(ElementName = "Parcels")]
        public Parcels Parcels { get; set; }

        [XmlElement(ElementName = "ConsignmentNo")]
        public string ConsignmentNumber { get; set; }

        [XmlElement(ElementName = "ConsigneeName")]
        public string ConsigneeName { get;  set; }
    }
}