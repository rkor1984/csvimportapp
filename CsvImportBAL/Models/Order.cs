﻿using System.Linq;
using System.Xml.Serialization;

namespace CsvImportBAL.Models
{
    [XmlRoot(ElementName = "Order")]
    public class Order
    {
        //needed for serialization
        public Order()
        {

        }

        public Order(IGrouping<string, ParcelItemViewModel> parcelItemGroup)
        {
            this.OrderNumber = parcelItemGroup.Key;
            var con = parcelItemGroup.ToList();
            var consignmentGroup=con.GroupBy(x => x.ConsignmentNumber);
            Consignments=new Consignments(consignmentGroup);
        }

        [XmlElement(ElementName = "Consignments")]
        public Consignments Consignments { get; set; }

        [XmlElement(ElementName = "OrderNo")]
        public string OrderNumber { get; set; }
    }
}