﻿using System;
using System.Collections.Generic;

namespace CsvImportBAL.CsvToXml
{
    public interface ICurrency
    {
        // get from database
        decimal ToGbp(decimal value, string currency);
    }
}