﻿using System.Collections.Generic;
using System.Linq;
using CsvImportBAL.Exceptions;

namespace CsvImportBAL.CsvToXml
{
    public class Currency: ICurrency
    {
        // get from database

        private Dictionary<string, decimal> _exchangeRate;

        public Currency()
        {
            _exchangeRate = new Dictionary<string, decimal>
            {
                {"",1 },
                {"eur",0.87m }
            };
        }

        public decimal ToGbp(decimal value, string currency)
        {
            if (!_exchangeRate.Keys.Contains(currency.ToLower()))
            {
                //add new currency to database if valid
                throw new InvalidCurrencyException("Currency Used is Invalid , Please Contact Support team!");
            }
            return value * _exchangeRate[currency];
        }
    }
}