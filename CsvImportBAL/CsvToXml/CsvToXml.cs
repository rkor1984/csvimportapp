﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CsvImportBAL.Exceptions;
using CsvImportBAL.Mappings;
using CsvImportBAL.Models;
using Validator = CsvImportBAL.Validation.Validator;

namespace CsvImportBAL.CsvToXml
{
    public class CsvToXml : ICsvToXml
    {
        private readonly ICurrency _currency;

        public CsvToXml(ICurrency currency)
        {
            _currency = currency;
        }

        public bool Process(string fullPath)
        {
            try
            {
                var (orders, totalValue, totalWeight) = CsvToOrderList(fullPath);
                SaveToDataBase(orders);
                OrdersToXml(orders, totalValue, totalWeight);
                return true;
            }
            catch (InValidDataProvidedException e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
            catch (InvalidCurrencyException e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        private void SaveToDataBase(List<Order> orders)
        {
            //save to database through DAL project
        }

        private ValueTuple<List<Order>,decimal,decimal> CsvToOrderList(string path)
        {
            var validator = new Validator();
            try
            {
                var totalValue = 0.0m;
                var totalWeight = 0.0m;

                var parcelItemViewModels = new List<ParcelItemViewModel>();

                var parcelItems = File.ReadAllLines(path);
                var mapping = (new OrderMap(parcelItems[0])).OrderMapDictionary;
                 for (var i = 1; i < parcelItems.Length; i++)
                {
                    var parcelItem = parcelItems[i].Split(',');
                    var value = validator.ValidateDecimal(parcelItem[mapping["item value"]]);
                    totalValue = +_currency.ToGbp(value,parcelItem[mapping["item currency"]]);
                    totalWeight =+validator.ValidateDecimal(parcelItem[mapping["item weight"]]);
                    parcelItemViewModels.Add(new ParcelItemViewModel(parcelItem, mapping));
                }

                var parcelItemGroups = parcelItemViewModels.GroupBy(x => x.OrderNumber);
                var orders = parcelItemGroups.Select(parcelItemGroup => new Order(parcelItemGroup)).ToList();

                return (orders.ToList(), totalValue, totalWeight);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private void OrdersToXml(List<Order> os, decimal totalValue, decimal totalWeight)
        {
            var orders = new Orders
            {
                OrderList = os,
                TotalValue = totalValue,
                TotalWeight = totalWeight
            };
            var serializer = new System.Xml.Serialization.XmlSerializer(typeof(Orders));
            var myWriter = new StreamWriter("C://Csv//myFileName.xml");
            serializer.Serialize(myWriter, orders);

            myWriter.Close();
        }
    }
}
