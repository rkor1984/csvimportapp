﻿using System;
using System.Collections.Generic;
using System.Text;
using CsvImportBAL.Models;

namespace CsvImportBAL.CsvToXml
{
    public interface ICsvToXml
    {
        bool Process(string fullPath);
    }
}
